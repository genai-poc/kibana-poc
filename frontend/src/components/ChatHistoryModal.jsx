import React from "react";

const ChatHistoryModal = ({ chatHistory, closeModal }) => {
  return (
    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 z-50 flex justify-center items-center">
      <div className="bg-white rounded-md shadow-md p-4 w-full max-w-lg relative">
        <button
          onClick={closeModal}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-700 focus:outline-none"
        >
          <svg
            className="h-6 w-6 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              fillRule="evenodd"
              d="M5.293 5.293a1 1 0 011.414 0L10 8.586l3.293-3.293a1 1 0 111.414 1.414L11.414 10l3.293 3.293a1 1 0 11-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 01-1.414-1.414L8.586 10 5.293 6.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <h2 className="text-xl font-medium mb-4">
          Chat History (Limited to 5 past queries)
        </h2>
        {chatHistory.length === 0 ? (
          <p className="text-gray-700">
            No chat history available at the moment.
          </p>
        ) : (
          <div className="max-h-72 overflow-y-auto">
            <ul className="space-y-4 chat-history">
              {chatHistory.slice(0, 5).map((entry, index) => (
                <li key={index} className="flex flex-col space-y-2">
                  <div className="flex">
                    <div className="rounded-lg bg-gray-200 p-2 max-w-xs">
                      <p className="text-gray-700">{entry.query}</p>
                    </div>
                  </div>
                  <div className="flex justify-end">
                    <div className="rounded-lg bg-blue-500 text-white p-2 max-w-xs">
                      <p>{entry.response}</p>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};

export default ChatHistoryModal;
