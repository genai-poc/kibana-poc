import React from "react";

const SkeletonCard = () => {
  return (
    <div className="bg-gray-200 p-4 rounded-md shadow-md font-mono">
      <div className="mb-4 h-4 bg-gradient-to-r from-sky-300 via-sky-200 to-sky-300 rounded-md animate-pulse w-3/4 transition-width duration-1000"></div>
      <div className="h-4 bg-gradient-to-r from-sky-300 via-sky-200 to-sky-300 rounded-md animate-pulse w-1/2 transition-width duration-1000"></div>
    </div>
  );
};

export default SkeletonCard;
