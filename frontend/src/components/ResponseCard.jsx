import React from "react";
import SkeletonCard from "./SkeletonCard";

const ResponseCard = ({ response, isLoading, query }) => {
  return (
    <div
      className={`bg-gray-100 p-4 rounded-md shadow-md font-mono ${
        response || isLoading ? "" : "hidden"
      }`}
    >
      {isLoading ? (
        <SkeletonCard />
      ) : (
        <>
          <p className="text-2xl mb-2 text-[#333] font-medium">{query}</p>
          <p className="text-lg mb-4 text-[#333] font-regular">{response}</p>
        </>
      )}
    </div>
  );
};

export default ResponseCard;
