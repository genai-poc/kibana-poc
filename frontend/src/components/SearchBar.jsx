import React, { useState } from "react";

const SearchBar = ({ onSearch }) => {
  const [query, setQuery] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(query);
    setQuery("");
  };
  return (
    <form
      onSubmit={handleSubmit}
      className="flex w-full my-4 font-mono h-[3.8rem] text-lg"
    >
      <input
        type="text"
        placeholder="Enter your question about the logs"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        className="rounded-md border border-gray-300 px-4 py-2 focus:outline-none focus:ring-1 focus:ring-blue-500 w-full"
      />
      <button
        type="submit"
        className="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md text-lg"
      >
        Search
      </button>
    </form>
  );
};

export default SearchBar;
