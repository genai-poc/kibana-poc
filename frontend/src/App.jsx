import React, { useState } from "react";
import SearchBar from "./components/SearchBar";
import ResponseCard from "./components/ResponseCard";
import ChatHistoryModal from "./components/ChatHistoryModal";
import SkeletonCard from "./components/SkeletonCard";
import axios from "axios";

const App = () => {
  const [userInput, setUserInput] = useState("");
  const [gptResponse, setGptResponse] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [chatHistory, setChatHistory] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleSearch = async (query) => {
    setIsLoading(true);
    setUserInput(query);
    try {
      const response = await axios.post(
        "http://localhost:5000/response",
        { message: query },
        { withCredentials: true }
      );
      console.log("response: ", response);
      if (!response) {
        throw new Error("Failed to get response");
      }
      setIsLoading(false);
      setGptResponse(response.data.output);
      setChatHistory((prevHistory) => [
        ...prevHistory,
        { query: query, response: response.data.output },
      ]);
    } catch (error) {
      console.error("Error fetching response:", error);
    }
  };

  const openChatHistory = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);

  return (
    <div className="container mx-auto px-4 py-3">
      <h1 className="text-6xl md:text-[10rem] font-extrabold bg-clip-text text-transparent bg-[linear-gradient(to_right,theme(colors.green.300),theme(colors.green.100),theme(colors.sky.400),theme(colors.yellow.200),theme(colors.sky.400),theme(colors.green.100),theme(colors.green.300))] bg-[length:200%_auto] animate-gradient font-poppins pb-8 text-center">
        Query
        <br className="block sm:hidden" />
        <span className="ml-4 sm:ml-0 block">
          {" "}
          your <br className="block sm:hidden" /> Logs{" "}
        </span>
      </h1>
      <SearchBar onSearch={handleSearch} />
      {isLoading ? (
        <SkeletonCard />
      ) : (
        <ResponseCard response={gptResponse} query={userInput} />
      )}
      <button
        className="text-blue-500 hover:underline font-medium mt-4"
        onClick={openChatHistory}
      >
        View Chat History
      </button>
      {isModalOpen && (
        <ChatHistoryModal
          chatHistory={chatHistory.slice(-5)} // displaying only the last 5 entries
          closeModal={closeModal}
        />
      )}
    </div>
  );
};

export default App;
