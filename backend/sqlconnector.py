import mysql.connector

mysql_host = "localhost"
mysql_user = "root"

# add your SQL password here
mysql_password = ""
mysql_database = "elasticsearch"

mysql_connection = mysql.connector.connect(
    host=mysql_host,
    user=mysql_user,
    password=mysql_password,
    database=mysql_database
)


def addLogsToTable(data):
    mysql_cursor = mysql_connection.cursor()
    noofRowsquery = "select count(*) from logs"
    mysql_cursor.execute(noofRowsquery)
    result = mysql_cursor.fetchone()
    newData = data["hits"]["hits"][:(len(data["hits"]["hits"])-result[0])]
    for hit in newData:
        _id = hit["_id"]
        timestamp = hit["_source"]["@timestamp"]
        message = hit["_source"]["message"]

        insert_query = "INSERT INTO logs (id, time, message) VALUES (%s, %s, %s)"
        insert_values = (_id, timestamp, message)
        mysql_cursor.execute(insert_query, insert_values)

    mysql_connection.commit()
    mysql_cursor.close()
