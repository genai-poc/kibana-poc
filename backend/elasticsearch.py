from langchain.agents import create_sql_agent
from langchain.agents.agent_toolkits import SQLDatabaseToolkit
from langchain.agents.agent_types import AgentType
from langchain.chat_models import ChatOpenAI
from langchain.sql_database import SQLDatabase
from langchain.prompts.chat import ChatPromptTemplate
from sqlalchemy import create_engine
import urllib.parse


def search(message):

    # add your SQL password here
    password = ""
    encoded_password = urllib.parse.quote_plus(password)
    cs = f"mysql+pymysql://root:{encoded_password}@localhost/elasticsearch"
    db_engine = create_engine(cs)
    db = SQLDatabase(db_engine)
    print(db.get_table_names())
    llm = ChatOpenAI(
        # add your open-ai key here
        temperature=0.0, openai_api_key="")
    sql_toolkit = SQLDatabaseToolkit(db=db, llm=llm)
    sql_toolkit.get_tools()
    prompt = ChatPromptTemplate.from_messages(
        [
            (
                "system",
                """
                you are a very intelligent ai assistent who is expert in identifying relevant questions from users and converting into sql queries to generate correct answer.
                Please use the below the context to write the sql queries, use mysql queries.
                context:
                you must queried against connected database. use the logs table.
                if more than 1 answer is there for particular question, return json array as output.
                """
            ),
            (
                "user", "{question}\ai:"
            )
        ]
    )
    agent = create_sql_agent(llm=llm, toolkit=sql_toolkit, agent_type=AgentType.ZERO_SHOT_REACT_DESCRIPTION,
                             verbose=True, max_execution_time=500, max_iterations=5000)
    ans = agent({'input': f'{message}'})
    return ans
