from flask import Flask, jsonify, request
import requests
import sqlconnector
import elasticsearch


app = Flask(__name__)
ELASTICSEARCH_URL = "http://localhost:9200"
INDEX_NAME = "test.logstash"

# add your elasticsearch password here in encoded format
HEADERS = {'Content-Type': 'application/json', 'Authorization': 'Basic '}


@app.route('/response', methods=['post'])
def response():
    request_body = request.get_json()
    print(request_body)
    add_logs_to_table()
    return elasticsearch.search(request_body['message'])


def add_logs_to_table():
    response = requests.get(
        f"{ELASTICSEARCH_URL}/{INDEX_NAME}/_search?size=1000", headers=HEADERS)
    logs = response.json()
    sqlconnector.addLogsToTable(logs)


if __name__ == '__main__':
    app.run(debug=True)
